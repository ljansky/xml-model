/**
 * Created by lukas on 17.8.15.
 */

var Q = require('q');
var fs = require("q-io/fs");
var parseString = require("xml2js").parseString;
var _ = require('lodash');
var error = require('./error');

var knexFactory = require('knex');
var bookshelfFactory = require('bookshelf');

var Repository = require('./repository');

var RepositoryLoader = function (connection) {
    var self = this;

    var knex = knexFactory({
        client: 'mysql',
        debug: false,
        connection: connection
    });

    this.bookshelf = bookshelfFactory(knex);

    this.RepositoryModel = this.bookshelf.Model.extend({
        tableName: 'repository'
    });

    this.modelConfigs = {};
    this.modelDefinitions = {};
    this.modelJoins = {};
    this.repositories = {};
    this.initialized = Q.defer();
    this.initializedBool = false;
    this.loadAllRepositories().then(function () {
        for (var i in self.modelConfigs) {
            if (self.modelConfigs.hasOwnProperty(i)) {
                self.repositories[i] = new Repository(self.modelConfigs[i], self.modelDefinitions[i], self.modelJoins[i], self.bookshelf);
            }
        }
        self.initialized.resolve();
        self.initializedBool = true;
    });
};

RepositoryLoader.prototype.loadAllRepositories = function () {
    var self = this;

    return new this.RepositoryModel().fetchAll().then(function (collection) {
        var promises = [];
        collection.forEach(function (repositoryInfo) {
            self.modelConfigs[repositoryInfo.get('name')] = {
                tableName: repositoryInfo.get('table'),
                ownerParent: repositoryInfo.get('owner_parent')
            };

            self.modelJoins[repositoryInfo.get('name')] = {};
        });

        collection.forEach(function (repositoryInfo) {
            promises.push(self.loadRepository(repositoryInfo.get('name')));
        });

        return Q.all(promises);
    });
};

RepositoryLoader.prototype.loadRepository = function (name) {
    var self = this;
    var path = './config/model/' + name + '.xml';

    return fs.isFile(path).then(function (res) {
        if (res) {
            return fs.read(path);
        } else {
            return false;
        }
    }).then(function (res) {
        if (res) {
            parseString(res, function (err, result) {
                self.modelDefinitions[name] = result.model;
                _.forEach(result.model.field, function (field) {
                    if (typeof field.$.foreign !== 'undefined') {
                        var foreignName = field.$.foreign;
                        self.modelConfigs[name][foreignName] = function () {
                            return this.belongsTo(self.repositories[foreignName].Model, field.$.name);
                        };

                        self.modelJoins[name][self.modelConfigs[foreignName].tableName] = {
                            type: 'belongsTo',
                            field: field.$.name
                        };

                        self.modelConfigs[foreignName][name] = function () {
                            return this.hasMany(self.repositories[name].Model, field.$.name);
                        }

                        self.modelJoins[foreignName][self.modelConfigs[name].tableName] = {
                            type: 'hasMany',
                            field: field.$.name
                        };
                    }

                    if (typeof field.$.owner !== 'undefined') {
                        var ownerName = field.$.owner;
                        self.modelConfigs[name]['_Owner'] = function () {
                            return this.belongsTo(self.repositories[ownerName].Model);
                        }
                    }
                });
            });
            console.log(name + ': loaded!');
        }
    });
};

RepositoryLoader.prototype.getRepositoryInfo = function (name) {
    return new this.RepositoryModel({name: name}).fetch();
};

RepositoryLoader.prototype.getRepository = function (name) {
    var self = this;
    return this.initialized.promise.then(function () {
        if (typeof self.repositories[name] !== 'undefined') {
            return self.repositories[name];
        } else {
            error.notFound('Resource not found');
        }
    })
};

RepositoryLoader.prototype.getStaticRepository = function (name) {
    if (this.initializedBool) {
        return this.repositories[name];
    } else {
        error.notFound('Static repository not found');
    }
};

RepositoryLoader.prototype.getAllRepositoriesInfo = function () {
    var self = this;
    return this.initialized.promise.then(function () {
        return self.modelConfigs;
    });
};

module.exports = RepositoryLoader;
