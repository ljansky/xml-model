/**
 * Created by lukas on 17.8.15.
 */

var Validator = require('./validator');
var Entity = require('./entity');
var Q = require('q');

var Repository = function (config, definition, joins, bookshelf) {

    this.joins = joins;
    this.config = config;
    this.Model = bookshelf.Model.extend(config);
    this.definition = definition;

    this.fields = {};
    this.ownerRepository = null;

    if (typeof this.definition !== 'undefined') {

        this.fields['id'] = {
            format: 1
        };

        for (var i = 0; i < this.definition.field.length; i++) {
            var field = this.definition.field[i];
            this.fields[field.$.name] = {
                format: field.$['default-format'] || 1,
                foreign: field.$['foreign'] || null
            };

            if (typeof field.$['owner'] !== 'undefined') {
                this.ownerRepository = '_Owner';
            }
        }
    }
};

Repository.prototype.createEntityFromModel = function (model) {
    return Entity.create(model, this.definition.$.name);
};

Repository.prototype.addRelatedOwner = function (related) {
    if (this.ownerRepository) {
        related.push(this.ownerRepository);
    }
};

Repository.prototype.findOne = function (params, related) {
    related = related || [];
    var self = this;

    this.addRelatedOwner(related);
    return new this.Model(params).fetch({
        withRelated: related
    }).then(function (model) {
        return self.createEntityFromModel(model);
    });
};

Repository.prototype.findById = function (id, related) {
    return this.findOne({
        id: id
    }, related);
};

Repository.prototype.addJoin = function (param, model) {
    var self = this;
    var splitted = param.split('.');

    if (splitted.length === 2 && typeof this.joins[splitted[0]] !== 'undefined') {
        model = model.query(function (qb) {
            if (self.joins[splitted[0]].type === 'belongsTo') {
                qb.leftJoin(splitted[0], splitted[0] + '.id', self.config.tableName + '.' + self.joins[splitted[0]].field);
            } else {
                qb.leftJoin(splitted[0], splitted[0] + '.' + self.joins[splitted[0]].field, self.config.tableName + '.id');
            }
        });
    }

    return model;
};

Repository.prototype.findAll = function (params, related) {
    var self = this;
    var where = params.where || [];
    var order = params.order || [];

    related = related || [];

    this.addRelatedOwner(related);

    var model = new this.Model();

    for (var i = 0; i < where.length; i++) {
        model = this.addJoin(where[i][0], model);
        model = model.query(function(qb) {
            if (where[i].length === 3) {
                qb.where(where[i][0], where[i][1], where[i][2]);
            } else {
                qb.whereRaw(where[i][0], where[i][1]);
            }

        })
    }

    for (var j = 0; j < order.length; j++) {
        if (order[j] === 'random') {
            model = model.query(function (qb) {
                qb.orderByRaw('RAND()');
            })
        } else {
            model = model.query(function (qb) {
                qb.orderBy(order[j][0], order[j][1]);
            })
        }
    }

    var limit = params.limit || 0;

    if (limit > 0) {
        model = model.query(function (qb) {
            qb.limit(parseInt(limit));
        })
    }

    var offset = params.offset || 0;
    if (offset > 0) {
        model = model.query(function (qb) {
            qb.offset(parseInt(offset));
        });
    }

    return model.fetchAll({
        withRelated: related
    }).then(function (models) {
        var entities = [];
        models.forEach(function (model) {
            entities.push(self.createEntityFromModel(model));
        });

        return entities;
    });
};

Repository.prototype.createEntity = function (data, ownerId, related) {
    var self = this;
    if (this.getFieldInfo('owner_id')) {
        data.owner_id = ownerId;
    }

    var newEntity =  new this.Model(data);

    related = related || [];
    if (this.ownerRepository) {
        related.push(this.ownerRepository);
    }

    var entityPromise;
    if (related.length > 0) {
        entityPromise = newEntity.load(related);
    } else {
        entityPromise = Q.fcall(function () {
            return newEntity;
        });
    }

    return entityPromise.then(function (entity) {
        return self.createEntityFromModel(entity);
    });
};

Repository.prototype.getValidator = function () {
    return new Validator(this.definition);
};

Repository.prototype.getFieldInfo = function (fieldName) {
    return this.fields[fieldName] || null;
};

Repository.prototype.getFields = function () {
    return this.fields;
};

module.exports = Repository;
