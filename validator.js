/**
 * Created by lukas on 19.8.15.
 */

var ruleFactory = require('./rules/ruleFactory');
var _ = require('lodash');
var error = require('./error');
var customValidator = require('./customValidator');

var Validator = function (definition) {
    this.definition = definition;
};

Validator.prototype.format = function (data) {
    //todo: unsetnout nemenitelna data
}

Validator.prototype.validate = function (data) {

    var self = this;
    var errors = [];

    var customValidation = customValidator.getValidation(this.definition.$.name);
    if (customValidation) {
        errors = errors.concat(customValidation(data));
    }

    _.forEach(this.definition.field, function (field) {
        if (typeof field.$.foreign !== 'undefined' && !parseInt(data[field.$.name])) {
            data[field.$.name] = null;
        }
        errors = errors.concat(self.validateField(field, data));
    });

    if (errors.length > 0) {
        error.badRequest(errors);
    }

    return errors;
};

Validator.prototype.validateField = function (field, data) {
    var errors = [];
    _.forEach(field.rule, function (ruleConfig) {
        var rule = ruleFactory.createRule(ruleConfig);

        var error = rule.resolve(field, data);
        if (error && error.length > 0) {
            errors.push({field: field.$.name, message: error[0]});
        }
    });

    return errors;
};

module.exports = Validator;
