/**
 * Created by lukas on 18.9.15.
 */

var Q = require('q');
var error = require('./error');

var Acl = function (xmlModel) {
    this.xmlModel = xmlModel;
};

Acl.prototype = {

    permissions: {},

    customPermissions: {},

    setCustomPermission: function (repository, customMethod) {
        this.customPermissions[repository] = customMethod;
    },

    getPermissions: function (userId) {

        var def = Q.defer();

        if (typeof this.permissions[userId] === 'undefined') {
            this.loadPermissions(userId).then(function (permissions) {
                def.resolve(permissions);
            });
        } else {
            def.resolve(this.permissions[userId]);
        }

        return def.promise;
    },

    loadPermissions: function (userId) {
        var self = this;

        return this.xmlModel.getResourceLoader().getAllRepositoriesInfo().then(function (repositories) {

            var permissions = {};

            for (var i in repositories) {
                if (repositories.hasOwnProperty(i)) {
                    permissions[i] = {
                        read: 0,
                        write: 0,
                        ownerParent: repositories[i].ownerParent,
                        roleParent: repositories[i].roleParent,
                        timeLock: null
                    }
                }
            }

            return self.getUserRoles(userId).then(function (roles) {
                return self.getRolesPermissions(roles);
            }).then(function (rolesPermissions) {
                rolesPermissions.forEach(function (permission) {
                    var repositoryName = permission.related('Repository').get('name');

                    if (permissions[repositoryName]['read'] < permission.get('read')) {
                        permissions[repositoryName]['read'] = permission.get('read');
                    }

                    if (permissions[repositoryName]['write'] < permission.get('write')) {
                        permissions[repositoryName]['write'] = permission.get('write');
                    }

                    permissions[repositoryName]['timeLock'] = permission.get('timeLock');
                });

                self.permissions[userId] = permissions;
                return permissions;
            });
        })
    },

    getUserRoles: function (userId) {

        var def = Q.defer();

        if (userId === 0) {
            def.resolve([1]);
        } else {
            this.xmlModel.getRepository('UserRole').then(function (repository) {
                return repository.findAll({
                    where: [['user_id', '=', userId]]
                })
            }).then(function (data) {
                var roles = [];

                data.forEach(function (role) {
                    roles.push(role.get('role_id'));
                });

                def.resolve(roles);
            });
        }

        return def.promise;
    },

    getRolesPermissions: function (roles) {
        return this.xmlModel.getRepository('RoleRepository').then(function (repository) {
            return repository.findAll({
                where: [['role_id', 'in', roles]]
            }, ['Repository']);
        });
    },

    isRepositoryAllowed: function (userId, repository, action) {
        return this.getPermissions(userId)
            .then(function (permissions) {

                if (typeof permissions[repository] === 'undefined') {
                    error.notFound('Resource not found');
                }

                if (permissions[repository][action] === 0) {
                    error.forbidden();
                }

                return permissions[repository][action] > 0;
            })
    },

    isEntityAllowed: function (userId, entity, action) {
        var self = this;
        return this.getPermissions(userId)
            .then(function (permissions) {
                var parent = entity;

                if (permissions[entity.getRepositoryName()].ownerParent) {
                    var parentRelations = permissions[entity.getRepositoryName()].ownerParent.split('.');
                    parentRelations.forEach(function (rel) {
                        parent = parent.related(rel);
                    });
                }

                var standardPermission = self.resolveEntityPermission(userId, parent, action, permissions[parent.getRepositoryName()]);
                var customPermission = true;

                if (typeof self.customPermissions[entity.getRepositoryName()] === 'function') {
                    customPermission = self.customPermissions[entity.getRepositoryName()].call(self, userId, entity, action);
                }

                var parentCustomPermission = true;

                if (typeof self.customPermissions[parent.getRepositoryName()] === 'function') {
                    parentCustomPermission = self.customPermissions[parent.getRepositoryName()].call(self, userId, parent, action);
                }

                return Q.all([standardPermission, customPermission, parentCustomPermission]);
            })
            .spread(function (standardPermission, customPermission, parentCustomPermission) {
                if (standardPermission && customPermission && parentCustomPermission) {
                    return entity;
                } else {
                    return null;
                }
            });
    },

    filterList: function (userId, data, action) {
        var self = this;
        var entityPromises = [];
        var filteredList = [];

        data.forEach(function (entity) {
            var entityPromise = self.isEntityAllowed(userId, entity, action)
                .then(function (entity) {
                    if (entity) {
                        filteredList.push(entity);
                    }
                });

            entityPromises.push(entityPromise);
        });

        return Q.all(entityPromises)
            .then(function () {
                return filteredList;
            });
    },

    resolveEntityPermission: function (userId, entity, action, permission) {
        //todo: pouzit customAcl (bude asi potreba ho mit v permission)
        if (permission[action] === 2) {
            return true;
        }
        //console.log(permission[action], userId, entity.getOwnerId());
        //todo: vyresit to jestli ma pravo pridavat nebo editovat
        if (permission[action] === 1 && (entity.getOwnerId() === 0 || entity.getOwnerId() === userId)) {
            return true;
        }

        return false;
    },

    removeUser: function (userId) {
        if (typeof this.permissions[userId] !== 'undefined') {
            delete this.permissions[userId];
        }
    }
};

module.exports = Acl;
