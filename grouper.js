/**
 * Created by lukas on 27.8.15.
 */

var _ = require('lodash');
var removeDiacritics = require('diacritics').remove;
var moment = require('moment');

var Grouper = function (groupInfo, titleResolver) {
    this.groupInfo = groupInfo;
    this.titleResolver = titleResolver;
}

var sortFunction = function (item) {
    return removeDiacritics(item.title).toUpperCase();
}

Grouper.prototype.getRelations = function () {
    var relations = [];
    for (var i = 0; i < this.groupInfo.length; i++) {
        if (this.groupInfo[i].type === 'foreign') {
            relations.push(this.groupInfo[i].repository);
        }
    }

    return relations;
}

Grouper.prototype.getFormat = function () {
    var format = {
        id: 1
    };

    var titleFields = this.titleResolver.getTitleFields();
    for (var i = 0; i < titleFields.length; i++) {
        format[titleFields[i]] = 1;
    }

    for (var i = 0; i < this.groupInfo.length; i++) {
        if (this.groupInfo[i].type === 'foreign') {
            var foreignFormat = {};
            foreignFormat[this.groupInfo[i].field] = 1;
            format[this.groupInfo[i].repository] = foreignFormat;
        }
    }

    return format;
}

Grouper.prototype.groupBy = function (items, groupsInfos) {
    var groups = {};
    var ret = [];
    var groupCounter = 0;

    if (groupsInfos.length === 0) {
        for (var i = 0; i < items.length; i++) {
            ret.push({
                id: items[i].id,
                title: this.titleResolver.getTitle(items[i])
            });
        }
    } else {
        var groupInfo = groupsInfos.shift();

        for (var i = 0; i < items.length; i++) {
            var groupName = this.groupName(items[i], groupInfo);
            if (typeof groups[groupName] === 'undefined') {
                groups[groupName] = groupCounter;
                ret[groupCounter] = {
                    title: groupName,
                    data: []
                };
                groupCounter++;
            }

            ret[groups[groupName]].data.push(items[i]);
        }

        for (var j in ret) {
            var tempGroupInfos = [];
            for (var k = 0; k < groupsInfos.length; k++) {
                tempGroupInfos.push(groupsInfos[k]);
            }
            ret[j].data = this.groupBy(ret[j].data, tempGroupInfos);
        }
    }

    ret = _.sortBy(ret, sortFunction);

    return ret;
}

Grouper.prototype.groupName = function (entity, group) {
    var name = '';

    switch (group.type) {
        case 'field':
            name = entity[group.field];
            break;
        case 'foreign':
            if (typeof entity[group.repository][group.field] === 'undefined' || !entity[group.repository][group.field]) {
                name = this.getDefaultGroupName(group);
            } else {
                name = entity[group.repository][group.field];
            }
            break;
        case 'firstLetter':
            name = entity[group.field].charAt(0).toUpperCase();
            break;
        case 'date':
            name = moment(entity[group.field]).format(group.format);
            break;
    }

    return name;
};

Grouper.prototype.getDefaultGroupName = function (group) {
    return group.default || 'Nezařazeno';
};

module.exports = Grouper;