/**
 * Created by lukas on 19.9.15.
 */

var error = {

    getError: function (code, messages) {
        return {
            code: code,
            messages: messages
        }
    },

    throwError: function (code, messages) {
        throw this.getError(code, messages);
    },

    notFound: function (message) {
        message = message || 'Not Found';
        this.throwError(404, [{field: null, message: message}]);
    },

    forbidden: function (message) {
        message = message || 'Forbidden';
        this.throwError(403, [{field: null, message: message}]);
    },

    unauthorized: function (message) {
        message = message || 'Unauthorized';
        this.throwError(401, [{field: null, message: message}]);
    },

    badRequest: function (messages) {
        this.throwError(400, messages);
    }
};

module.exports = error;
