var Acl = require('./acl');
var entity = require('./entity');
var error = require('./error');
var Formatter = require('./formatter');
var Grouper = require('./grouper');
var repository = require('./repository');
var RepositoryLoader = require('./repositoryLoader');
var TitleResolver = require('./titleResolver');
var validator = require('./validator');
var customValidator = require('./customValidator');

var XmlModel = function (connection) {
    this.resourceLoader = new RepositoryLoader(connection);
    this.acl = new Acl(this);
};

XmlModel.prototype = {

    getResourceLoader: function () {
        return this.resourceLoader;
    },

    getRepository: function (resourceName) {
        return this.getResourceLoader().getRepository(resourceName);
    },

    getFormatter: function (format) {
        if (typeof format === 'undefined') {
            return Formatter;
        } else {
            return new Formatter(format, this.getResourceLoader());
        }
    },

    getAcl: function () {
        return this.acl;
    },

    getError: function () {
        return error;
    },

    getGrouper: function (groups, titleResolver) {
        return new Grouper(groups, titleResolver);
    },

    getTitleResolver: function (titleInfo) {
        return new TitleResolver(titleInfo);
    },

    getCustomValidator: function () {
        return customValidator;
    }
};

var xmlModel = null;

module.exports = function (connection) {
    if (!xmlModel) {
        xmlModel = new XmlModel(connection);
    }

    return xmlModel;
};
