/**
 * Created by lukas on 19.9.15.
 */

//var repositoryLoader = require('./repositoryLoader');

var Entity = function (model, repositoryName) {
    this.model = model;
    this.repositoryName = repositoryName;
};

Entity.create = function (model, repositoryName) {
    return new Entity(model, repositoryName);
}

Entity.prototype = {
    model: null,

    tempData: null,

    save: function (data) {
        return this.model.save(data);
    },

    delete: function () {
        return this.model.destroy();
    },

    get: function (name) {
        if (typeof name === 'undefined') {
            return this.model;
        } else if (typeof value === 'undefined') {
            return this.model.get(name);
        }
    },

    setTempData: function (data) {
        this.tempData = data;
    },

    getTempData: function (name) {
        if (typeof name === 'undefined') {
            return this.tempData;
        } else {
            if (this.tempData && typeof this.tempData[name] !== 'undefined') {
                return this.tempData[name];
            } else {
                return null;
            }
        }
    },

    related: function (repositoryName) {
        var related = this.model.related(repositoryName);

        if (typeof related === 'undefined') {
            return null;
        }


        if (typeof related.models !== 'undefined') {
            var entities = [];
            for (var i = 0; i < related.models.length; i++) {
                entities.push(Entity.create(related.models[i], repositoryName));
            }

            return entities;
        } else {
            return Entity.create(related, repositoryName);
        }
    },

    getRepositoryName: function () {
        return this.repositoryName;
    },

    getRepository: function () {
        return repositoryLoader.getStaticRepository(this.getRepositoryName());
    },

    getOwnerId: function () {
        var ownerId = this.get('owner_id') || 0;

        if (ownerId === 0) {
            var ownerEntity = this.related('_Owner');
            if (ownerEntity) {
                ownerId = ownerEntity.get('owner_id');
            }
        }

        return ownerId;
    }
};

module.exports = Entity;
