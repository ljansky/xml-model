'use strict';

var customValidator = {

    validations: {},

    setValidation: function (repositoryName, validationFunction) {
        this.validations[repositoryName] = validationFunction;
    },

    getValidation: function (repositoryName) {
        if (typeof this.validations[repositoryName] === 'function') {
            return this.validations[repositoryName];
        } else {
            return null;
        }
    }
}

module.exports = customValidator;
