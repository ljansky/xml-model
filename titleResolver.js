/**
 * Created by lukas on 27.8.15.
 */
'use strict';

var TitleResolver = function (titleInfo) {
    this.titleInfo = titleInfo;
};

TitleResolver.prototype.getTitle = function (entity) {
    var title = [];
    for (var i = 0; i < this.titleInfo.length; i++) {
        title.push(entity[this.titleInfo[i]]);
    }

    return title.join(' ');
};

TitleResolver.prototype.getTitleFields = function () {
    return this.titleInfo;
};

module.exports = TitleResolver;