/**
 * Created by lukas on 12.10.15.
 */

var util = require('util');
var Rule = require('./rule');

var RuleFilled = function (config, factory) {
    RuleFilled.super_.call(this, config, factory);
};

util.inherits(RuleFilled, Rule);

RuleFilled.prototype.isValid = function (value) {
    if (!value || value == 0) {
        return false;
    }

    return true;
};

module.exports = RuleFilled;