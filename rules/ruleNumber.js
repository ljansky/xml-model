/**
 * Created by lukas on 8.10.15.
 */

var util = require('util');
var Rule = require('./rule');

var RuleNumber = function (config, factory) {
    RuleNumber.super_.call(this, config, factory);
    this.min = config.$.min || null;
    this.max = config.$.max || null;
};

util.inherits(RuleNumber, Rule);

RuleNumber.prototype.isValid = function (value) {
    if (!isNaN(value) && parseInt(value) % 1 === 0) {
        var parsed = parseInt(value);
        if (this.min && parsed < this.min) {
            return false;
        }

        if (this.max && parsed > this.max) {
            return false;
        }

        return true;
    } else {
        return false;
    }
};

module.exports = RuleNumber;