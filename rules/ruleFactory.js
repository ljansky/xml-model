/**
 * Created by lukas on 7.9.15.
 */
'use strict';

var RuleDate = require('./ruleDate');
var RuleNumber = require('./ruleNumber');
var RuleFilled = require('./ruleFilled');

var ruleFactory = {
    createRule: function (config) {
        var type = config.$.type;

        switch (type) {
            case 'date':
                return new RuleDate(config, ruleFactory);
            case 'number':
                return new RuleNumber(config, ruleFactory);
            case 'filled':
                return new RuleFilled(config, ruleFactory);
        }
    }
};

module.exports = ruleFactory;