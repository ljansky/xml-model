/**
 * Created by lukas on 12.10.15.
 */

var Rule = function (config, factory) {
    this.config = config;
    this.factory = factory;
    this.rules = [];
    this.setRules();
};

Rule.prototype.getError = function () {
    return this.config.$['message'];
};

Rule.prototype.setRules = function () {
    var self = this;
    if (this.config.rule) {
        this.config.rule.forEach(function (rule) {
            self.rules.push(self.factory.createRule(rule));
        });
    }
};

Rule.prototype.resolveRules = function (field, data) {
    var errors = [];
    this.rules.forEach(function (rule) {
        var ruleErrors = rule.resolve(field, data);
        if (ruleErrors) {
            errors = errors.concat(ruleErrors);
        }
    });

    return errors.length > 0 ? errors : null;
};

Rule.prototype.resolve = function (field, data) {
    var localField = this.config.$.field || field.$.name;
    this.getForeignValue(field, data);

    var valid = this.isValid(data[localField]);

    if (this.rules.length > 0) {
        if (valid) {
            return this.resolveRules(field, data);
        } else {
            return null;
        }

    } else {
        return valid ? null : [this.getError()];
    }
};

Rule.prototype.getForeignValue = function (field, data) {
    //console.log('entity', data);
};

Rule.prototype.isValid = function (value) {
    return true;
};

module.exports = Rule;