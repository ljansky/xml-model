/**
 * Created by lukas on 12.10.15.
 */

var util = require('util');
var Rule = require('./rule');

var RuleDate = function (config, factory) {
    RuleDate.super_.call(this, config, factory);
};

util.inherits(RuleDate, Rule);

RuleDate.prototype.isValid = function (value) {
    //console.log('date:', value);
    return true;
};

module.exports = RuleDate;