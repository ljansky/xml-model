/**
 * Created by lukas on 19.8.15.
 */

var moment = require('moment');

var Formatter = function (formatConfig, repositoryLoader) {
    this.formatConfig = formatConfig;
    this.repositoryLoader = repositoryLoader;
};

Formatter.decodeFormat = function (format) {
    if (typeof format === 'undefined') {
        return null;
    } else if (typeof format === 'string') {
        return JSON.parse(format);
    } else {
        return format;
    }
};

Formatter.prototype.formatAll = function (data) {
    var self = this;
    var ret = [];
    data.forEach(function (entity) {
        ret.push(self.format(entity));
    });

    return ret;
};

Formatter.prototype.format = function (entity) {
    var formatConfig;

    if (!this.formatConfig) {
        formatConfig = this.getDefaultFormatConfig(entity);
    } else {
        formatConfig = this.formatConfig;
    }

    var ret = {};
    for (var i in formatConfig) {
        if (formatConfig.hasOwnProperty(i)) {
            if (typeof formatConfig[i] === 'object') {
                var formatter = new Formatter(formatConfig[i], this.repositoryLoader);
                var related = entity.related(i);

                if (Array.isArray(related)) {
                    ret[i] = [];
                    for (var j = 0; j < related.length; j++) {
                        ret[i].push(formatter.format(related[j]));
                    }
                } else {
                    ret[i] = formatter.format(related);
                }
            } else {
                if (formatConfig[i] === 1) {
                    //todo: toto nejak vyresit
                    var fieldInfo = this.repositoryLoader.getStaticRepository(entity.getRepositoryName()).getFieldInfo(i);

                    if (fieldInfo) {
                        ret[i] = this.formatValue(entity.get(i), fieldInfo['format']);
                    }
                } else {
                    ret[i] = this.formatValue(entity.get(i), formatConfig[i]);
                }

            }
        }
    }

    return ret;
};

Formatter.prototype.getRelations = function (prefix) {
    if (typeof prefix === 'undefined') {
        prefix = '';
    } else {
        prefix = prefix + '.';
    }

    var relations = [];
    for (var i in this.formatConfig) {
        if (this.formatConfig.hasOwnProperty(i)) {
            if (typeof this.formatConfig[i] === 'object') {
                relations.push(prefix + i);
                var formatter = new Formatter(this.formatConfig[i]);
                relations = relations.concat(formatter.getRelations(prefix + i));
            }
        }
    }

    return relations;
};

Formatter.prototype.formatValue = function (value, format) {
    if (format === 'date') {
        return moment(value).format('YYYY-MM-DD');
    } else if (format === 'dateHuman') {
        return moment(value).format('D. M. YYYY');
    } else {
        return value;
    }
};

Formatter.prototype.getDefaultFormatConfig = function (entity) {
    var fields = this.repositoryLoader.getStaticRepository(entity.getRepositoryName()).getFields();
    var format = {};
    for (var i in fields) {
        if (fields.hasOwnProperty(i)) {
            format[i] = fields[i].format;
        }
    }

    return format;
};

module.exports = Formatter;